package tateti;

import java.util.Scanner;

public class Tateti {

	private char tablero[][];
	private char jugador;
	private char ganador;

	public Tateti() {
		tablero = new char[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				tablero[i][j] = '_';
			}
		}

		jugador = 'x';
		ganador = ' ';
	}

	public void mostrar() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(tablero[i][j] + "  ");
			}
			System.out.println();
		}
		System.out.println();
	}

	public char próximoTurno() {
		return jugador;
	}

	public void jugar(int i, int j) {
		if (tablero[i][j] != '_') {
			throw new RuntimeException("posición no vacía: [" + i + "][" + j + "]");
		}

		if (terminado()) {
			throw new RuntimeException("juego ya terminado");
		}

		tablero[i][j] = jugador;
		jugador = (jugador == 'x') ? 'o' : 'x';
	}

	public boolean terminado() {
		return hayGanador() || completo();
	}

	public boolean hayGanador() {
		char c;

		// miramos filas
		for (int i = 0; i < 3; i++) {
			c = tablero[i][0];
			if (c == tablero[i][1] && c == tablero[i][2] && c != '_') {
				ganador = c;
				return true;
			}
		}

		// miramos columnas
		for (int j = 0; j < 3; j++) {
			c = tablero[0][j];
			if (c == tablero[1][j] && c == tablero[2][j] && c != '_') {
				ganador = c;
				return true;
			}
		}

		// miramos una diagonal
		c = tablero[0][0];
		if (c == tablero[1][1] && c == tablero[2][2] && c != '_') {
			ganador = c;
			return true;
		}

		// miramos la otra diagonal
		c = tablero[0][2];
		if (c == tablero[1][1] && c == tablero[2][0] && c != '_') {
			ganador = c;
			return true;
		}

		return false;
	}

	public boolean completo() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (tablero[i][j] == '_') {
					return false;
				}
			}
		}
		return true;
	}

	public char ganador() {
		if (!terminado()) {
			throw new RuntimeException("el juego no terminó todavía");
		}

		return ganador;
	}

	public static void main(String args[]) {
		Tateti tateti = new Tateti();
		Scanner input = new Scanner(System.in);

		while (!tateti.terminado()) {
			int i = 0, j = 0;

			System.out.println("Le toca a " + tateti.próximoTurno());

			while (!(1 <= i && i <= 3)) {
				System.out.print(" > dame la fila    [1-3]: ");
				i = input.nextInt();
			}

			while (!(1 <= j && j <= 3)) {
				System.out.print(" > dame la columna [1-3]: ");
				j = input.nextInt();
			}

			tateti.jugar(i - 1, j - 1); // obs: no checkeamos errores :'(
			tateti.mostrar();
		}

		if (tateti.hayGanador()) {
			System.out.println("Ganó: " + tateti.ganador());
		} else {
			System.out.println("Empate!");
		}
	}

}
